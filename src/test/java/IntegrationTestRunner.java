import integration.SqlConnectorServiceIntTest;
import org.junit.runner.JUnitCore;

public class IntegrationTestRunner {
    public static void main(String[] args) {
        JUnitCore.runClasses(
                SqlConnectorServiceIntTest.class
        );
    }
}
