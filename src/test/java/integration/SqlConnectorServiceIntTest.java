package integration;

import model.*;
import org.junit.Assert;
import org.junit.Test;
import service.SqlConnectorService;
import util.ResourceProvider;
import util.StringProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SqlConnectorServiceIntTest {
    private final SqlConnectorService sqlConnectorService;

    public SqlConnectorServiceIntTest() {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
        }

        sqlConnectorService = new SqlConnectorService(properties);
    }

    //tests for users
    @Test
    public void testGetUsers() {
        List<User> userList = this.sqlConnectorService.getUsers();
        userList.forEach(Assert::assertNotNull);
    }

    @Test
    public void testGetUserByUsername() {
        String username = "test";
        Assert.assertNotNull(sqlConnectorService.getUserByUsername(username));
        username = "false test";
        Assert.assertNull(sqlConnectorService.getUserByUsername(username));
    }

    @Test
    public void testGetUserById() {
        List<User> userList = this.sqlConnectorService.getUsers();
        userList.forEach(user -> Assert.assertNotNull(this.sqlConnectorService.getUserById(user.getId())));
    }

    @Test
    public void testSaveAndDeleteUserAndPatient() {
        User user = new User();
        Patient patient;
        String randomUsername = ((Long) System.currentTimeMillis()).toString();
        user.setUsername(randomUsername);
        Assert.assertNotNull(this.sqlConnectorService.saveUser(user));
        user = this.sqlConnectorService.getUserByUsername(randomUsername);
        patient = this.sqlConnectorService.getPatientByUserId(user.getId());
        Assert.assertNotNull(this.sqlConnectorService.deletePatient(patient));
        Assert.assertNotNull(this.sqlConnectorService.deleteUser(user));
    }

    @Test
    public void testGetPatientByUserIdAndGetDoctorByUserId() {
        List<User> userList = this.sqlConnectorService.getUsers();
        userList.forEach(user -> {
            if (StringProvider.ROLE_PATIENT.equals(user.getRoleString())) {
                Assert.assertNotNull(this.sqlConnectorService.getPatientByUserId(user.getId()));
            } else {
                Assert.assertNotNull(this.sqlConnectorService.getDoctorByUserId(user.getId()));
            }
        });
    }

    //tests for patients
    @Test
    public void testGetPatientDataList() {
        List<PatientData> patientDataList = this.sqlConnectorService.getPatientDataList();
        patientDataList.forEach(Assert::assertNotNull);
    }

    @Test
    public void testGetPatientById() {
        List<User> userList = this.sqlConnectorService.getUsers();
        List<Patient> patientList = new ArrayList<>();
        userList.forEach(user -> {
            if (StringProvider.ROLE_PATIENT.equals(user.getRoleString())) {
                patientList.add(this.sqlConnectorService.getPatientByUserId(user.getId()));
            }
        });
        patientList.forEach(patient -> Assert.assertNotNull(this.sqlConnectorService.getPatientById(patient.getIdPatient())));
    }

    @Test
    public void testGetPatientComboBoxList() {
        Assert.assertNotNull(this.sqlConnectorService.getPatientComboBoxList());
    }

    //tests for consultancies
    @Test
    public void testGetConsultancyList() {
        List<Consultancy> consultancyList = this.sqlConnectorService.getConsultancyList();
        consultancyList.forEach(Assert::assertNotNull);
    }

    @Test
    public void testGetConsultancyDomainList() {
        List<String> domainList = this.sqlConnectorService.getConsultancyDomainList();
        domainList.forEach(Assert::assertNotNull);
    }

    @Test
    public void testSaveAndDeleteConsultancy() {
        Consultancy consultancy = new Consultancy();
        consultancy = this.sqlConnectorService.saveConsultancy(consultancy);
        Assert.assertNotNull(consultancy);
        Assert.assertNotNull(this.sqlConnectorService.deleteConsultancy(consultancy));
    }

    //tests for analyses
    @Test
    public void testGetAnalysisList() {
        List<Analysis> analysisList = this.sqlConnectorService.getAnalysisList(14);
        analysisList.forEach(Assert::assertNotNull);
    }

    @Test
    public void testSaveAndDeleteAnalysis() {
        Analysis analysis = new Analysis();
        analysis.setIdDoctor(1);
        analysis.setIdPatient(2);
        analysis.setIdConsultancy(1);
        analysis = this.sqlConnectorService.saveAnalysis(analysis);
        Assert.assertNotNull(analysis);
        Assert.assertNotNull(this.sqlConnectorService.deleteAnalysis(analysis));
    }
}
