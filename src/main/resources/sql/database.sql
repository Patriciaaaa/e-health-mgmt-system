CREATE database EHealth;

use EHealth;

#drop table users;
#drop table patients;
#drop table Consultancies;
#drop table analyses
#drop table doctors

CREATE TABLE users (
                       id		     INT               PRIMARY KEY auto_increment,
                       username      varchar(50)       NOT NULL,
                       password      varchar(50)       NOT NULL,
                       birthDate     DATE              NOT NULL,
                       email         varchar(50)       NOT NULL,
                       phoneNumber   varchar(12)       NOT NULL,
                       gender	     varchar(20)	   NOT NULL,
                       role          varchar(10)       NOT NULL
);

CREATE TABLE doctors (
                         idDoctor        INT              PRIMARY KEY auto_increment,
                         idUser          INT,
                         domain          varchar(50)      NOT NULL,
                         availability    BIT                 NOT NULL
);

CREATE TABLE patients (
                          idPatient       INT              PRIMARY KEY auto_increment,
                          idUser          INT,
                          height          FLOAT,
                          weight          FLOAT,
                          bloodType       varchar(2),
                          remark          varchar(500)
);

CREATE TABLE analyses (
                          idAnalysis        INT              PRIMARY KEY auto_increment,
                          idDoctor          INT			   NOT NULL,
                          idPatient         INT			   NOT NULL,
                          idConsultancy     INT			   NOT NULL,
                          analysisDate      DATETIME		   NOT NULL,
                          domain            varchar(50)        NOT NULL,
                          remark            varchar(500)
);

CREATE TABLE consultancies (
                               idConsultancy       INT              PRIMARY KEY auto_increment,
                               name                varchar(50)         NOT NULL,
                               price               FLOAT             NOT NULL,
                               domain              varchar(50)         NOT NULL
);

ALTER TABLE doctors
    ADD FOREIGN KEY (idUser) REFERENCES users(id);
ALTER TABLE patients
    ADD FOREIGN KEY (idUser) REFERENCES users(id);
ALTER TABLE analyses
    ADD FOREIGN KEY (idDoctor) REFERENCES doctors(idDoctor);
ALTER TABLE analyses
    ADD FOREIGN KEY (idPatient) REFERENCES patients(idPatient);
ALTER TABLE analyses
    ADD FOREIGN KEY (idConsultancy) REFERENCES consultancies(idConsultancy);

#patient users
insert into users(username, password, birthDate, email, phoneNumber, gender, role)
	values('Emily White', 'asd', '2011-01-11', 'emily.white@yahoo.com','+40758952663', 'Female', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('John Doe', 'asd', '2001-09-02', 'john.doe@yahoo.com','+40744444444', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Emma Long', 'asd', '1989-11-25', 'emma.long@yahoo.com','+40758952663', 'Female', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Phil Maguire', 'asd', '1967-06-30', 'phil.maguire2@gmail.com','+40723165987', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Joshua Schwartz', 'asd', '1945-02-19', 'joshua.schwartz@yahoo.com','+40722141811', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Christina Kneiphoff', 'asd', '1966-09-05', 'christi_kneiphoff3@gmail.com','+40766987546', 'Female', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Lisa Brown', 'asd', '2002-03-22', 'lisa.brown@gmail.com','+40765432432', 'Female', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Timothy Douglas', 'asd', '2000-04-14', 'tim.douglas@yahoo.com','+40755667788', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Frank Furt', 'asd', '1999-05-26', 'frankfurtthetown@gmail.com','+40711121314', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Chad McParty', 'asd', '1995-01-01', 'chadtheparty@hotmail.com','+40742042042', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Barbara Chaudry', 'asd', '1970-07-20', 'chaudry.barbara4@yahoo.com','+40726165649', 'Female', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Marcus Neustadt', 'asd', '1950-01-16', 'marcus.neustadt@yahoo.com','+40733252811', 'Male', 2);

insert into users(username, password, birthDate, email, phoneNumber, gender, role)
values('Josh McGinn', 'asd', '1989-01-06', 'josh.mcginn@gmail.com','+40774586611', 'Male', 2);

#users doctor
insert into users(username, password, birthDate, email, phoneNumber, gender, role)
	values('John Borrow', 'doctor', '1975-12-16', 'john.borrow@gmail.com','+40763654123', 'Male', 1);

insert into doctors(idUser, domain, availability)
values(14, 'Neurosurgery', 1);

#patients
INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (1,1.66,65,'B', 'Folly words widow one downs few age every seven. If miss part by fact he park just shew. Discovered had get considered projection who favourable. Necessary up knowledge it tolerably. Unwilling departure education is be dashwoods or an.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (2,1.78,68,'AB', 'Use off agreeable law unwilling sir deficient curiosity instantly. Easy mind life fact with see has bore ten. Parish any chatty can elinor direct for former. Up as meant widow equal an share least.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (3,1.70,62,'AB', 'Consulted he eagerness unfeeling deficient existence of. Calling nothing end fertile for venture way boy.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (4,1.54,71,'A', 'Esteem spirit temper too say adieus who direct esteem.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (5,1.40,60,'A', 'It esteems luckily mr or picture placing drawing no.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (6,1.69,68,'A', 'Apartments frequently or motionless on reasonable projecting expression.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (7,1.48,65,'B', 'Way mrs end gave tall walk fact bed.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (8,1.54,85,'0', 'Death there mirth way the noisy merit.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (9,1.64,80,'B', 'Piqued shy spring nor six though mutual living ask extent.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (10,1.47,90,'B', 'Replying of dashwood advanced ladyship smallest disposal or.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (11,1.50,74,'AB', 'Attempt offices own improve now see.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (12,1.48,64,'0', 'Called person are around county talked her esteem.');

INSERT INTO patients(idUser,height,weight,bloodType, remark)
VALUES (13,1.69,84,'A', 'Those fully these way nay thing seems.');


INSERT INTO Consultancies(name,price,domain)
VALUES ('Regular health check',120,'Family Medicine');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Regular health check',200,'Cardiology');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Regular health check',250,'Neurosurgery');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Regular health check',120,'Ophthalmology');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Regular health check',100,'Gynecology');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Lens change',200,'Ophthalmology');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Therapy',160,'Neurosurgery');

INSERT INTO Consultancies(name,price,domain)
VALUES ('Sickness diagnosis',80,'Family Medicine');


INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,4,2,'2022-03-16','Cardiology', 'Fact are size cold why had part.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,3,2,'2022-04-04','Cardiology', 'Two among sir sorry men court. Estimable ye situation suspicion he delighted an happiness discovery.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,8,6,'2022-05-29','Ophthalmology', 'In friendship diminution instrument so. Son sure paid door with say them.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,10,4,'2022-02-18','Ophthalmology', 'Of acceptance insipidity remarkably is invitation.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,11,5,'2022-05-31','Gynecology', 'Ecstatic followed handsome drawings entirely mrs one yet outweigh.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,5,5,'2022-04-16','Gynecology', 'Whatever boy her exertion his extended.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,2,5,'2022-04-02','Gynecology', 'Bed one supposing breakfast day fulfilled off depending questions.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,3,5,'2022-04-26','Gynecology', 'Dependent on so extremely delivered by. Yet ﻿no jokes worse her why.');
INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,11,3,'2022-05-25','Neurosurgery', 'Demesne far hearted suppose venture excited see had has.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,3,7,'2022-06-19','Neurosurgery', 'Enjoyment discourse ye continued pronounce we necessary abilities.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,7,8,'2022-05-28','Family Medicine', 'Possession themselves sentiments apartments devonshire we of do discretion.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,9,1,'2022-06-13','Family Medicine', 'Increasing travelling own simplicity you astonished expression boisterous.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,2,'2021-08-20','Cardiology', 'You fully seems stand nay own point walls.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,5,'2021-11-03','Gynecology', 'Mr excellence inquietude conviction is in unreserved particular.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,1,'2021-08-01','Family Medicine', 'It as instrument boisterous frequently apartments an in.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,2,'2021-11-21','Ophthalmology', 'Projection invitation affronting admiration if no on or.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,5,'2021-10-12','Neurosurgery', 'Terminated resolution no am frequently collecting insensible he do appearance.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,4,'2021-12-23','Ophthalmology', 'Cold in late or deal.');

INSERT INTO analyses(idDoctor,idPatient,idConsultancy,analysisDate,domain, remark)
VALUES (1,1,1,'2021-11-30','Cardiology', 'Announcing of invitation principles in.');

select * from users;
select * from patients;
select distinct consultancies.domain from consultancies;
select * from doctors where doctors.idUser = 14;
SELECT * FROM users, patients WHERE users.id = patients.idPatient;
select * from analyses;
SELECT Count(analyses.idAnalysis) FROM analyses;
