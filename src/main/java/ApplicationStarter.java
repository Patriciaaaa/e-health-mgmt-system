import com.sun.javafx.application.LauncherImpl;
import controller.PreloaderController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import controller.BaseController;
import service.InitPreloader;
import util.ResourceProvider;

import java.io.IOException;

public class ApplicationStarter extends Application {

    @Override
    public void init() {
        InitPreloader init = new InitPreloader();
        init.checkFunctions();
    }

    public static void main(String[] args) {
        LauncherImpl.launchApplication(ApplicationStarter.class, PreloaderController.class, args);
    }

    @Override
    public void start(Stage mainStage) throws IOException {
        BaseController baseController = new BaseController();
        Scene loginScene = new Scene(
                ResourceProvider.loadFxml("fxml/login-stage.fxml"), 500, 500
        );
        loginScene.getStylesheets().add(
                ResourceProvider.loadCss("css/style.css")
        );

        mainStage.setScene(loginScene);
        mainStage.setTitle("E-Health Management System");
        mainStage.setResizable(false);
        mainStage.show();
        baseController.setMainStage(mainStage);
    }
}
