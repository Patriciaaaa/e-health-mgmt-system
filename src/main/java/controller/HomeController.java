package controller;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import service.UserService;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeController extends BaseController implements Initializable {

    public Button signOutButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setUserData();
    }

    public void signOut() {
        super.changeScene(signOutButton, "login-stage.fxml", "Login");
    }

    void setUserData() {
        super.setCurrentUser(UserService.getInstance().getUser());
    }
}
