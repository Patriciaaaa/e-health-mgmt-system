package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import model.User;
import service.UserService;
import util.ColorProvider;
import util.StringProvider;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends BaseController implements Initializable {

    @FXML
    Node root;
    public Label usernameLabel;
    public TextField usernameField;
    public Label usernameErrorLabel;
    public Label passwordLabel;
    public PasswordField passwordField;
    public Label passwordErrorLabel;
    public Label credentialsErrorLabel;
    public Button registrationButton;
    public Button loginButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.usernameField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(this.usernameField, this.usernameLabel, this.usernameErrorLabel));
        this.passwordField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(this.passwordField, this.passwordLabel, this.passwordErrorLabel));
        super.initSqlConnectorService();
        this.root.setOnKeyPressed(k -> {
            if (k.getCode().equals(KeyCode.ENTER)) {
                login();
            }
        });
    }

    @FXML
    private void handleMenuButtonAction () {
        super.changeScene(registrationButton, "registration-stage.fxml", "Register");
    }

    public void login() {
        if (!usernameField.getText().equals("")) {
            User user = super.getSqlConnectorService().getUserByUsername(this.usernameField.getText());

            if (user == null || !user.getPassword().equals(this.passwordField.getText())) {
                credentialsErrorLabel.setText(StringProvider.invalidCredentials);
                credentialsErrorLabel.setTextFill(ColorProvider.DARK_RED);
            } else {
                UserService userService = UserService.getInstance();
                userService.setUser(user);
                if (StringProvider.ROLE_DOCTOR.equals(userService.getUser().getRoleString())) {
                    super.changeScene(loginButton, "doctor-home-stage.fxml", "Home");
                } else {
                    super.changeScene(loginButton, "patient-home.fxml", "Home");
                }
            }
        } else {
            usernameErrorLabel.setText(StringProvider.emptyString);
            usernameErrorLabel.setTextFill(ColorProvider.DARK_RED);
            usernameLabel.setTextFill(ColorProvider.DARK_RED);
        }
    }
}
