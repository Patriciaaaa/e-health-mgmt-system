package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class PatientServicesController extends BaseController implements Initializable {
    @FXML
    public Button returnButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {}

    public void returnToHome() {
        super.changeScene(returnButton, "patient-home.fxml", "Patient");
    }
}
