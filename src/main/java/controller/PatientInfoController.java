package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import model.Patient;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class PatientInfoController extends HomeController implements Initializable {

    private Patient patient;
    private ArrayList<String> tipOfTheDay;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.setUserData();
        super.initSqlConnectorService();
        this.patient = super.getSqlConnectorService().getPatientByUserId((super.getCurrentUser().getId()));
        this.setMenuTitleText(location);

        try {
            tipOfTheDay = readTipsMessages("src/main/resources/static/tipmessage.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        this.setTipOfTheDay();
    }

    private ArrayList<String> readTipsMessages(String filename) throws FileNotFoundException {
        ArrayList<String> result = new ArrayList<>();

        try (FileReader f = new FileReader(filename)) {
            StringBuffer sb = new StringBuffer();
            while (f.ready()) {
                char c = (char) f.read();
                if (c == '\n') {
                    result.add(sb.toString());
                    sb = new StringBuffer();
                } else {
                    sb.append(c);
                }
            }
            if (sb.length() > 0) {
                result.add(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void setTipOfTheDay() {
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(tipOfTheDay.size());
        String it = tipOfTheDay.get(index);
        tipOfTheDayLabel.setText("Tip of the day: " + it);
    }

    @FXML
    Button signOutButton;
    @FXML
     Button filesButton;
    @FXML
     Button schedulingButton;
    @FXML
     Button doctorsButton;
    @FXML
     Button servicesButton;
    @FXML
    Label welcome_message;
    @FXML
    Label tipOfTheDayLabel;

    private void setMenuTitleText(URL location) {
        if (location.toString().contains("patient-home.fxml")) {
            this.welcome_message.setText("Welcome " + super.getCurrentUser().getUsername() + "!");
        }
    }

    public void goToFiles() {

        super.changeScene(filesButton, "patient-files.fxml", "Files");
    }

    public void goToScheduling() {

        super.changeScene(schedulingButton, "patient-scheduling.fxml", "Scheduling");
    }

    public void goToDoctors() {

        super.changeScene(doctorsButton, "patient-services.fxml", "Doctors");
    }

    public void goToServices() {

        super.changeScene(servicesButton, "patient-prices.fxml", "Services");
    }

    public void signOut() {

        super.changeScene(signOutButton, "login-stage.fxml", "Login");
    }
}
