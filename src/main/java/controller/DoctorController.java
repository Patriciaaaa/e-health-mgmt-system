package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import model.*;
import service.AnalysisService;
import service.ConsultancyService;
import service.PatientService;
import util.RegexProvider;
import util.StringProvider;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class DoctorController extends HomeController {
    //menu option buttons and table views
    public Label menuTitle;
    public Button patientListButton;
    public Button appointmentsButton;
    public Button pricesButton;
    public TableView<PatientData> patientTableView;
    public TableView<Analysis> analysisTableView;
    public TableView<Consultancy> consultancyTableView;
    public Button homeButton;
    //Analysis form elements
    public Label patientComboBoxLabel;
    public ComboBox<String> patientComboBox;
    public Label patientErrorLabel;
    public Label consultancyComboBoxLabel;
    public ComboBox<String> consultancyComboBox;
    public Label consultancyErrorLabel;
    public Label analysisDateLabel;
    public DatePicker analysisDatePicker;
    public Label analysisDateErrorLabel;
    public Label remarkLabel;
    public TextArea remarkTextArea;
    public Button createAppointmentButton;
    //Consultancy form elements
    public Label nameLabel;
    public TextField nameField;
    public Label nameErrorLabel;
    public Label priceLabel;
    public TextField priceField;
    public Label priceErrorLabel;
    public Label domainLabel;
    public ComboBox<String> domainComboBox;
    public Label domainErrorLabel;
    public Button createConsultancyButton;
    //services and logged in doctor
    private Doctor doctor;
    private PatientService patientService;
    private ConsultancyService consultancyService;
    private AnalysisService analysisService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.setUserData();
        super.initSqlConnectorService();
        this.doctor = super.getSqlConnectorService().getDoctorByUserId(super.getCurrentUser().getId());
        this.setMenuTitleText(location);

        if (location.toString().contains("doctor-patient-list-stage.fxml")) {
            patientService = new PatientService();
            this.loadPatientData();
            this.patientTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
                if (newSelection != null) {
                    this.goToPatientProfile(newSelection.getIdPatient());
                }
            });
        }

        if (location.toString().contains("doctor-appointments-stage.fxml")) {
            patientService = new PatientService();
            consultancyService = new ConsultancyService();
            analysisService = new AnalysisService();
            this.loadAnalysisData(this.doctor.getId());
            this.setPatientComboBoxValues();
            this.setConsultancyComboBoxValues();
            this.assignAnalysisFormListeners();
            this.analysisDatePicker.setDayCellFactory(d ->
                    new DateCell() {
                        @Override
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);
                            setDisable(item.isBefore(LocalDate.now()));
                        }});
        }

        if (location.toString().contains("doctor-consultancies-stage.fxml")) {
            consultancyService = new ConsultancyService();
            this.loadConsultancyData();
            this.setDomainComboBoxValues();
            this.assignConsultancyFormListeners();
        }
    }

    public void goToPatientList() {
        super.changeScene(signOutButton, "doctor-patient-list-stage.fxml", "Patient list");
    }

    public void goToPatientProfile(int patientId) {
        Patient patient = this.patientService.getPatientById(patientId);
        super.initSqlConnectorService();
        User patientAssociatedUser = super.getSqlConnectorService().getUserById(patient.getIdUser());
        System.out.println(patient);
        System.out.println(patientAssociatedUser);
    }

    public void goToAppointments() {
        super.changeScene(signOutButton, "doctor-appointments-stage.fxml", "Appointments");
    }

    public void goToConsultancies() {
        super.changeScene(signOutButton, "doctor-consultancies-stage.fxml", "Consultancies and Prices");
    }

    public void goToHome() {
        super.changeScene(signOutButton, "doctor-home-stage.fxml", "Home");
    }

    public void createAppointment() {
        if (this.validateAppointmentData()) {
            Analysis analysis = new Analysis();
            String[] consultancyIdDomainName =
                    consultancyComboBox.getSelectionModel().getSelectedItem().split(" - ");
            String[] patientIdUsername = patientComboBox.getSelectionModel().getSelectedItem().split(" - ");
            analysis.setIdDoctor(this.doctor.getId());
            analysis.setIdConsultancy(Integer.parseInt(consultancyIdDomainName[0]));
            analysis.setDomain(consultancyIdDomainName[1]);
            analysis.setConsultancyName(consultancyIdDomainName[2]);
            analysis.setIdPatient(Integer.parseInt(patientIdUsername[0]));
            analysis.setPatientName(patientIdUsername[1]);
            analysis.setAnalysisDate(Date.valueOf(this.analysisDatePicker.getValue()));
            analysis.setRemark(this.remarkTextArea.getText());
            analysis = this.analysisService.saveAnalysis(analysis);

            if (analysis != null) {
                ObservableList<Analysis> analysisObservableArray = this.analysisTableView.getItems();
                analysisObservableArray.add(analysis);
                this.analysisTableView.setItems(analysisObservableArray);
                this.patientComboBox.getSelectionModel().select(0);
                super.clearErroneousField(this.patientComboBoxLabel, this.patientErrorLabel);
                this.consultancyComboBox.getSelectionModel().select(0);
                super.clearErroneousField(this.consultancyComboBoxLabel, this.consultancyErrorLabel);
                this.analysisDatePicker.getEditor().setText("");
                this.analysisDatePicker.setValue(null);
                super.clearErroneousField(this.analysisDateLabel, this.analysisDateErrorLabel);
                this.remarkTextArea.setText("");
            }
        }
    }

    public void createConsultancy() {
        if (validateConsultancyData()) {
            Consultancy consultancy = new Consultancy();
            consultancy.setName(this.nameField.getText());
            consultancy.setPrice(Float.parseFloat(this.priceField.getText()));
            consultancy.setDomain(this.domainComboBox.getSelectionModel().getSelectedItem());
            consultancy = this.consultancyService.saveConsultancy(consultancy);

            if (consultancy != null) {
                ObservableList<Consultancy> consultancyObservableList = this.consultancyTableView.getItems();
                consultancyObservableList.add(consultancy);
                this.consultancyTableView.setItems(consultancyObservableList);
                this.nameField.setText("");
                super.clearErroneousField(this.nameLabel, this.nameErrorLabel);
                this.priceField.setText("");
                super.clearErroneousField(this.priceLabel, this.priceErrorLabel);
                this.domainComboBox.getSelectionModel().select(0);
                super.clearErroneousField(this.domainLabel, this.domainErrorLabel);
            }
        }
    }

    public void verifyLength() {
        String text = this.remarkTextArea.getText();

        if (text.length() > 500) {
            this.remarkTextArea.setText(text.substring(0, 500));
        }
    }

    private void assignAnalysisFormListeners() {
        this.analysisDatePicker.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledDatePicker(
                        this.analysisDatePicker,
                        this.analysisDateLabel,
                        this.analysisDateErrorLabel));
        this.consultancyComboBox.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledComboBox(
                        this.consultancyComboBox,
                        this.consultancyComboBoxLabel,
                        this.consultancyErrorLabel));
        this.patientComboBox.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledComboBox(
                        this.patientComboBox,
                        this.patientComboBoxLabel,
                        this.patientErrorLabel));
    }

    private void assignConsultancyFormListeners() {
        this.nameField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(
                        this.nameField,
                        this.nameLabel,
                        this.nameErrorLabel));
        this.priceField.textProperty().addListener((observable, oldValue, newValue) -> {
            super.labeledTextField(this.priceField, this.priceLabel, this.priceErrorLabel);

            if (!this.priceField.getText().matches(RegexProvider.floatNumberRegex)) {
                super.markErroneousField(this.priceLabel, this.priceErrorLabel, StringProvider.invalidPrice);
            }
        });

        this.domainComboBox.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledComboBox(
                        this.domainComboBox,
                        this.domainLabel,
                        this.domainErrorLabel));
    }

    private Boolean validateAppointmentData() {
        if (this.consultancyComboBox.getSelectionModel().getSelectedIndex() == 0) {
            super.markErroneousField(this.consultancyComboBoxLabel, this.consultancyErrorLabel);
        }

        if (this.patientComboBox.getSelectionModel().getSelectedIndex() == 0) {
            super.markErroneousField(this.patientComboBoxLabel, this.patientErrorLabel);
        }

        if (this.analysisDatePicker.getValue() == null) {
            super.markErroneousField(this.analysisDateLabel, this.analysisDateErrorLabel);
            return false;
        }

        return true;
    }

    private Boolean validateConsultancyData() {
        if ("".equals(this.nameField.getText())) {
            super.markErroneousField(this.nameLabel, this.nameErrorLabel);
            return false;
        }

        if ("".equals(this.priceField.getText())) {
            super.markErroneousField(this.priceLabel, this.priceErrorLabel);
            return false;
        }

        if (!this.priceField.getText().matches(RegexProvider.floatNumberRegex)) {
            super.markErroneousField(this.priceLabel, this.priceErrorLabel, StringProvider.invalidPrice);
            return false;
        }

        if (this.domainComboBox.getSelectionModel().getSelectedIndex() == 0) {
            super.markErroneousField(this.domainLabel, this.domainErrorLabel);
            return false;
        }

        return true;
    }

    private void loadPatientData() {
        List<PatientData> patientDataList = this.patientService.getPatientDataList();
        this.patientTableView.setEditable(false);
        this.patientTableView.setItems(FXCollections.observableArrayList(patientDataList));
    }

    private void loadAnalysisData(int idDoctor) {
        List<Analysis> analysisList = this.analysisService.getAnalysisList(idDoctor);
        this.analysisTableView.setEditable(false);
        this.analysisTableView.setItems(FXCollections.observableArrayList(analysisList));
    }

    private void loadConsultancyData() {
        this.consultancyTableView.setEditable(false);
        this.consultancyTableView.setItems(FXCollections.observableArrayList(findAllConsultancies()));
    }

    private List<Consultancy> findAllConsultancies() {
        return this.consultancyService.getConsultancyList();
    }

    private void setPatientComboBoxValues() {
        List<String> patientIdAndNameList = new ArrayList<>();
        patientIdAndNameList.add("Select an element");
        patientService.getPatientComboBoxList().forEach(
                patient -> patientIdAndNameList.add(patient.getId() + " - " + patient.getName())
        );
        this.patientComboBox.setItems(FXCollections.observableArrayList(patientIdAndNameList));
        this.patientComboBox.getSelectionModel().select(0);
    }

    private void setConsultancyComboBoxValues() {
        List<String> consultancyIdAndNameList = new ArrayList<>();
        consultancyIdAndNameList.add("Select an element");
        this.findAllConsultancies().forEach(
                consultancy -> consultancyIdAndNameList.add(
                        consultancy.getId() + " - " + consultancy.getDomain() + " - " +  consultancy.getName()
                )
        );
        this.consultancyComboBox.setItems(FXCollections.observableArrayList(consultancyIdAndNameList));
        this.consultancyComboBox.getSelectionModel().select(0);
    }

    private void setDomainComboBoxValues() {
        List<String> domainList = new ArrayList<>();
        domainList.add("Select an element");
        this.consultancyService.getConsultancyDomainList().forEach(consultancyDomain -> {
            if (!domainList.contains(consultancyDomain.replace(" ", ""))) {
                domainList.add(consultancyDomain);
            }
        });
        this.domainComboBox.setItems(FXCollections.observableArrayList(domainList));
        this.domainComboBox.getSelectionModel().select(0);
    }

    private void setMenuTitleText(URL location) {
        if (location.toString().contains("doctor-home-stage.fxml")) {
            this.menuTitle.setText("Welcome dr. " + super.getCurrentUser().getUsername() + "!");
        }

        if (location.toString().contains("doctor-patient-list-stage.fxml")) {
            this.menuTitle.setText("Patient List");
        }

        if (location.toString().contains("doctor-consultancies-stage.fxml")) {
            this.menuTitle.setText("Consultancies and Prices");
        }

        if (location.toString().contains("doctor-appointments-stage.fxml")) {
            this.menuTitle.setText("Appointments");
        }
    }
}
