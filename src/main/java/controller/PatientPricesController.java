package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class PatientPricesController extends BaseController implements Initializable {

    @FXML
    public Button returnButton;
    @FXML
    public Button pricesList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {}

    public void returnToHome() {
        super.changeScene(returnButton, "patient-home.fxml", "Patient");
    }

    public void goToPrices() {
        super.changeScene(pricesList, "3rd-interface-patient.fxml", "Prices");
    }
}