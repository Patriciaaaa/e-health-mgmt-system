package controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import model.User;
import service.UserService;
import util.RegexProvider;
import util.StringProvider;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class RegistrationController extends BaseController implements Initializable {
    public Label usernameLabel;
    public TextField usernameField;
    public Label usernameErrorLabel;
    public Label passwordLabel;
    public PasswordField passwordField;
    public Label passwordErrorLabel;
    public Label confirmPasswordLabel;
    public PasswordField confirmPasswordField;
    public Label confirmPasswordErrorLabel;
    public Button registerButton;
    public Button backButton;
    public Label birthDateLabel;
    public DatePicker birthDatePicker;
    public Label birthDateErrorLabel;
    public Label emailLabel;
    public TextField emailField;
    public Label emailErrorLabel;
    public Label phoneNumberLabel;
    public TextField phoneNumberField;
    public Label phoneNumberErrorLabel;
    public Label genderLabel;
    public ComboBox<String> genderComboBox;
    public Label genderComboBoxErrorLabel;
    @FXML
    public Node root;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.usernameField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(this.usernameField, this.usernameLabel, this.usernameErrorLabel));
        this.passwordField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(this.passwordField, this.passwordLabel, this.passwordErrorLabel));
        this.confirmPasswordField.textProperty().addListener((observable, oldValue, newValue) ->
                super.labeledTextField(this.confirmPasswordField, this.confirmPasswordLabel, this.confirmPasswordErrorLabel));
        this.birthDatePicker.setDayCellFactory(d ->
                new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        setDisable(item.isAfter(LocalDate.now()));
                    }});
        this.birthDatePicker.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledDatePicker(this.birthDatePicker, this.birthDateLabel, this.birthDateErrorLabel));
        this.emailField.textProperty().addListener((observable, oldValue, newValue) -> {
                    if (this.emailField.getText().length() != 0) {
                        if (!this.emailField.getText().matches(RegexProvider.emailRegex)) {
                            super.markErroneousField(this.emailLabel, this.emailErrorLabel);
                            this.emailErrorLabel.setText(StringProvider.invalidEmail);
                        } else {
                            this.emailErrorLabel.setText("");
                            super.clearErroneousField(this.emailLabel, this.emailErrorLabel);
                        }
                    } else {
                        super.markErroneousField(this.emailLabel, this.emailErrorLabel);
                    }
                }
        );
        this.phoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
                    if (this.phoneNumberField.getText().length() != 0) {
                        if (!this.phoneNumberField.getText().matches(RegexProvider.phoneNumberRegex)) {
                            super.markErroneousField(this.phoneNumberLabel, this.phoneNumberErrorLabel);
                            this.phoneNumberErrorLabel.setText(StringProvider.invalidPhoneNumber);
                        } else {
                            this.phoneNumberErrorLabel.setText("");
                            super.clearErroneousField(this.phoneNumberLabel, this.phoneNumberErrorLabel);
                        }
                    } else {
                        super.markErroneousField(this.phoneNumberLabel, this.phoneNumberErrorLabel);
                    }
                }
        );
        this.genderComboBox.setItems(FXCollections.observableArrayList("Select one", "Male", "Female"));
        this.genderComboBox.getSelectionModel().select(0);
        this.genderComboBox.valueProperty().addListener((observable, oldValue, newValue) ->
                super.labeledComboBox(
                        this.genderComboBox,
                        this.genderLabel,
                        this.genderComboBoxErrorLabel));
        this.root.setOnKeyPressed(k -> {
            if (k.getCode().equals(KeyCode.ENTER)) {
                register();
            }
        });
        super.initSqlConnectorService();
    }

    public void register() {
        if (this.validateForm()) {
            User user = new User();
            user.setUsername(this.usernameField.getText());
            user.setPassword(this.passwordField.getText());
            user.setBirthDate(Date.valueOf(this.birthDatePicker.getValue()));
            user.setEmail(this.emailField.getText());
            user.setPhoneNumber(this.phoneNumberField.getText());
            user.setGender(this.genderComboBox.getValue());
            user.setRoleCode(2);
            user = super.getSqlConnectorService().saveUser(user);
            UserService userService = UserService.getInstance();
            userService.setUser(user);
            if (user != null) {
                super.changeScene(this.registerButton, "patient-home.fxml", "Home");
            }
        }
    }

    public void goToLogin() {
        this.clearFields();
        super.changeScene(this.registerButton, "login-stage.fxml", "Login");
    }

    private void clearFields() {
        this.clearPasswordFields();
        this.clearUserNameField();
    }

    private void clearUserNameField() {
        this.usernameField.setText("");
        super.clearErroneousField(this.usernameLabel, this.usernameErrorLabel);
    }

    private void clearPasswordFields() {
        this.passwordField.setText("");
        this.confirmPasswordField.setText("");
        super.clearErroneousField(this.passwordLabel, this.passwordErrorLabel);
        super.clearErroneousField(this.confirmPasswordLabel, this.confirmPasswordErrorLabel);
    }

    private boolean validateForm() {
        if (this.usernameField.getText().equals("")) {
            super.markErroneousField(this.usernameLabel, this.usernameErrorLabel);
            return false;
        }

        if (!this.passwordField.getText().equals(this.confirmPasswordField.getText())) {
            super.markErroneousField(this.passwordLabel);
            super.markErroneousField(this.confirmPasswordLabel,
                    this.confirmPasswordErrorLabel,
                    StringProvider.mismatchBetweenPasswords);
            return false;
        }

        if (this.passwordField.getText().equals("")) {
            super.markErroneousField(this.passwordLabel);
            return false;
        }

        if (this.confirmPasswordField.getText().equals("")) {
            super.markErroneousField(this.confirmPasswordLabel, this.confirmPasswordErrorLabel);
            return false;
        }

        if (super.getSqlConnectorService().getUserByUsername(this.usernameField.getText()) != null) {
            super.markErroneousField(this.usernameLabel, this.usernameErrorLabel, StringProvider.usernameAlreadyTaken);
            return false;
        }

        if (this.birthDatePicker.getValue() == null) {
            super.markErroneousField(this.birthDateLabel, this.birthDateErrorLabel);
            return false;
        }

        if (this.emailField.getText().length() == 0) {
            super.markErroneousField(this.emailLabel, this.emailErrorLabel);
            return false;
        }

        if (this.phoneNumberField.getText().length() == 0) {
            super.markErroneousField(this.phoneNumberLabel, this.phoneNumberErrorLabel);
            return false;
        }

        if (this.genderComboBox.getSelectionModel().getSelectedIndex() == 0) {
            super.markErroneousField(this.genderLabel, this.genderComboBoxErrorLabel);
            return false;
        }

        return true;
    }
}
