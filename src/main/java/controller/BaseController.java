package controller;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.User;
import service.SqlConnectorService;
import util.AlertProvider;
import util.ColorProvider;
import util.ResourceProvider;
import util.StringProvider;

import java.util.Arrays;
import java.util.Properties;

public class BaseController {
    User currentUser;
    Stage mainStage;
    SqlConnectorService sqlConnectorService;

    public void changeScene(Button pressedButton, String fileName, String title) {
        Parent myNewScene;
        mainStage = null;

        try {
            mainStage = (Stage) pressedButton.getScene().getWindow();
            myNewScene = ResourceProvider.loadFxml("fxml/" + fileName);
            Scene scene = new Scene(myNewScene);
            mainStage.setScene(scene);
            mainStage.setTitle(title);
            mainStage.show();
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        }
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    protected void initSqlConnectorService() {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
            return;
        }

        this.sqlConnectorService = new SqlConnectorService(properties);
    }

    protected void labeledTextField(TextField field, Label label, Label errorLabel) {
        if (field.getText().length() == 0) {
            this.markErroneousField(label, errorLabel);
        } else {
            this.clearErroneousField(label, errorLabel);
        }
    }

    protected void labeledDatePicker(DatePicker datePicker, Label label, Label errorLabel) {
        if (datePicker.getValue() == null) {
            this.markErroneousField(label, errorLabel);
        } else {
            this.clearErroneousField(label, errorLabel);
        }
    }

    protected void labeledComboBox(ComboBox<String> combobox, Label label, Label errorLabel) {
        if (combobox.getSelectionModel().getSelectedIndex() == 0) {
            this.markErroneousField(label, errorLabel);
        } else {
            this.clearErroneousField(label, errorLabel);
        }
    }

    protected void markErroneousField(Label label, Label errorLabel) {
        errorLabel.setText(StringProvider.emptyString);
        errorLabel.setTextFill(ColorProvider.DARK_RED);
        label.setTextFill(ColorProvider.DARK_RED);
    }

    protected void markErroneousField(Label label, Label errorLabel, String errorMessage) {
        errorLabel.setText(errorMessage);
        errorLabel.setTextFill(ColorProvider.DARK_RED);
        label.setTextFill(ColorProvider.DARK_RED);
    }

    protected void markErroneousField(Label label) {
        label.setTextFill(ColorProvider.DARK_RED);
    }

    protected void clearErroneousField(Label label, Label errorLabel) {
        errorLabel.setText("");
        errorLabel.setTextFill(Color.BLACK);
        label.setTextFill(Color.BLACK);
    }

    public SqlConnectorService getSqlConnectorService() {
        return sqlConnectorService;
    }

    public void setSqlConnectorService(SqlConnectorService sqlConnectorService) {
        this.sqlConnectorService = sqlConnectorService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
