package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class PatientFileController extends BaseController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    Button returnButton;

    public void returnToHome() {

        super.changeScene(returnButton, "patient-home.fxml", "Patient");
    }
}
