package controller;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

import java.net.URL;
import java.util.ResourceBundle;

public class ThirdInterfacePatientController extends BaseController implements Initializable {

    public Button returnButton;
    public Label Consultancy1Label;
    public Label Consultancy2Label;
    public Label Consultancy3Label;
    public Label Consultancy4Label;
    public Label Consultancy5Label;
    public Label Consultancy6Label;
    public Label Consultancy7Label;
    public Label Consultancy8Label;
    public Label Price1Label;
    public Label Price2Label;
    public Label Price3Label;
    public Label Price4Label;
    public Label Price5Label;
    public Label Price6Label;
    public Label Price7Label;
    public Label Price8Label;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        toolTip();
    }

    public void goBack() {

        super.changeScene(this.returnButton, "patient-prices.fxml", "Prices");
    }

    public void toolTip(){
        Tooltip Consultancy1ExtraText = new Tooltip("Regular health check-ups can identify any early signs of health issues.");
        Consultancy1Label.setTooltip(Consultancy1ExtraText);

        Tooltip Consultancy2ExtraText = new Tooltip("Recommended when unknown symptoms appear.");
        Consultancy2Label.setTooltip(Consultancy2ExtraText);

        Tooltip Consultancy3ExtraText = new Tooltip("Regular health check-ups can identify any early signs of health issues.");
        Consultancy3Label.setTooltip(Consultancy3ExtraText);

        Tooltip Consultancy4ExtraText = new Tooltip("Regular health check-ups can identify any early signs of health issues.");
        Consultancy4Label.setTooltip(Consultancy4ExtraText);

        Tooltip Consultancy5ExtraText = new Tooltip("Recommended after neurosurgery to speed up recovery time.");
        Consultancy5Label.setTooltip(Consultancy5ExtraText);

        Tooltip Consultancy6ExtraText = new Tooltip("Regular health check-ups can identify any early signs of health issues.");
        Consultancy6Label.setTooltip(Consultancy6ExtraText);

        Tooltip Consultancy7ExtraText = new Tooltip("Diopters can change as eye adapts so lens need to be adapted as well.");
        Consultancy7Label.setTooltip(Consultancy7ExtraText);

        Tooltip Consultancy8ExtraText = new Tooltip("Regular health check-ups can identify any early signs of health issues.");
        Consultancy8Label.setTooltip(Consultancy8ExtraText);
    }

    public void handleMouseEnter1() {
        opacityZero();
        Price1Label.setOpacity(1);
    }
    public void handleMouseEnter2() {
        opacityZero();
        Price2Label.setOpacity(1);
    }
    public void handleMouseEnter3() {
        opacityZero();
        Price3Label.setOpacity(1);
    }
    public void handleMouseEnter4() {
        opacityZero();
        Price4Label.setOpacity(1);
    }
    public void handleMouseEnter5() {
        opacityZero();
        Price5Label.setOpacity(1);
    }
    public void handleMouseEnter6() {
        opacityZero();
        Price6Label.setOpacity(1);
    }
    public void handleMouseEnter7() {
        opacityZero();
        Price7Label.setOpacity(1);
    }
    public void handleMouseEnter8() {
        opacityZero();
        Price8Label.setOpacity(1);
    }

    public void handleMouseExit() {
        Price1Label.setOpacity(1);
        Price2Label.setOpacity(1);
        Price3Label.setOpacity(1);
        Price4Label.setOpacity(1);
        Price5Label.setOpacity(1);
        Price6Label.setOpacity(1);
        Price7Label.setOpacity(1);
        Price8Label.setOpacity(1);
    }

    public void opacityZero() {
        Price1Label.setOpacity(0);
        Price2Label.setOpacity(0);
        Price3Label.setOpacity(0);
        Price4Label.setOpacity(0);
        Price5Label.setOpacity(0);
        Price6Label.setOpacity(0);
        Price7Label.setOpacity(0);
        Price8Label.setOpacity(0);

    }
}
