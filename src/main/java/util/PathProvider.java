package util;

public interface PathProvider {
    public final String staticPath = "./static/";
    public final String physicalResourcesPath = "./src/main/resources/";
}
