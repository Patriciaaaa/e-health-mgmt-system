package util;

public interface StringProvider {
    public final String ROLE_DOCTOR = "DOCTOR";
    public final String ROLE_PATIENT = "PATIENT";
    public final String emptyString = "This field is mandatory.";
    public final String invalidDate = "Invalid date.";
    public final String invalidEmail = "Invalid e-mail address.";
    public final String invalidPhoneNumber = "Invalid phone number.";
    public final String mismatchBetweenPasswords = "The passwords do not match.";
    public final String usernameAlreadyTaken = "Username already taken.";
    public final String invalidCredentials = "Username or password is invalid.";
    public final String invalidPrice = "Price is invalid.";
    public final String contactAdministrator = "An error has occurred. Please contact your administrator.";
    public final String welcome = "Welcome ";
    public final String profileDetails = "Profile details";
}
