package util;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public interface ColorProvider {
    public final Paint DARK_RED = Color.color(0.6745, 0.01177, 0.01177, 1.0);
}
