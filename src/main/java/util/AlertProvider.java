package util;

import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;

public interface AlertProvider {
    public static void displayAlert(String alertTitle, String alertHeader, String alertContent, Alert.AlertType alertType) {
        getAlert(alertTitle, alertHeader, alertContent, alertType).showAndWait();
    }

    public static Alert getAlert(String alertTitle, String alertHeader, String alertContent, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(alertTitle);
        alert.setHeaderText(alertHeader);
        TextArea area = new TextArea(alertContent);
        area.setWrapText(true);
        area.setEditable(false);
        alert.getDialogPane().setContent(area);
        alert.setResizable(true);
        return alert;
    }
}