package util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Paths;

public class ResourceProvider {
    public static BufferedInputStream loadConfiguration()
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        BufferedInputStream bufferedInputStream = (BufferedInputStream) classLoader.getResourceAsStream(
                PathProvider.staticPath + "configuration.properties");

        if (bufferedInputStream == null) {
            throw new IllegalArgumentException("configuration.properties not found");
        }

        return bufferedInputStream;
    }

    public static Parent loadFxml(String fileName) throws IOException {
        return FXMLLoader.load(Paths.get(PathProvider.physicalResourcesPath + fileName).toUri().toURL());
    }

    public static String loadCss(String fileName) throws IOException {
        return Paths.get(PathProvider.physicalResourcesPath + fileName).toUri().toURL().toString();
    }
}
