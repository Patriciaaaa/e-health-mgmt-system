package service;

import javafx.scene.control.Alert;
import model.Patient;
import model.PatientComboBoxData;
import model.PatientData;
import util.AlertProvider;
import util.ResourceProvider;
import util.StringProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PatientService {

    private SqlConnectorService sqlConnectorService;

    public PatientService() {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
            return;
        }

        this.sqlConnectorService = new SqlConnectorService(properties);
    }

    public Patient getPatientById(int patientId) {
        return this.sqlConnectorService.getPatientById(patientId);
    }

    public List<PatientData> getPatientDataList() {
        return this.sqlConnectorService.getPatientDataList();
    }

    public List<PatientComboBoxData> getPatientComboBoxList() {
        return this.sqlConnectorService.getPatientComboBoxList();
    }

    public SqlConnectorService getSqlConnectorService() {
        return sqlConnectorService;
    }

    public void setSqlConnectorService(SqlConnectorService sqlConnectorService) {
        this.sqlConnectorService = sqlConnectorService;
    }
}
