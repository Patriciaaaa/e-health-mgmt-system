package service;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class InitPreloader implements Initializable {

    public Label lblLoading;
    public static Label lblLoadingg;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lblLoadingg=lblLoading;
    }

    public String checkFunctions(){

        final String[] message = {""};
        Thread t1 = new Thread(() -> {
            message[0] = "Loading interface";
            Platform.runLater(() -> lblLoadingg.setText(message[0]));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            message[0] = "Loading DB";
            Platform.runLater(() -> lblLoadingg.setText(message[0]));
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t3 = new Thread(() -> {
            message[0] = "Open Main Stage";
            Platform.runLater(() -> lblLoadingg.setText(message[0]));
        });

        try {
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
            t3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return message[0];
    }

}
