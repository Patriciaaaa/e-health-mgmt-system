package service;

import javafx.scene.control.Alert;
import model.*;
import util.AlertProvider;
import util.StringProvider;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class SqlConnectorService {
    String selectAllUsers = "SELECT * FROM users";
    Properties databaseConfig;

    public SqlConnectorService(Properties databaseProperties) {
        this.databaseConfig = databaseProperties;
    }

    public User getUserByUsername(String username) {
        Connection connection = this.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM users WHERE users.username = '" + username + "'");
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return decomposeUserAttributes(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public User getUserById(int userId) {
        Connection connection = this.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM users WHERE users.id = '" + userId + "'");
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return decomposeUserAttributes(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public List<User> getUsers() {
        List<User> userList = new ArrayList<>();

        try {
            Connection connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(selectAllUsers);
            ResultSet rs = ps.executeQuery();
            User user;

            while (rs.next()) {
                user = decomposeUserAttributes(rs);
                userList.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        }

        return userList;
    }

    public User saveUser(User savedUser) {
        Connection connection = this.getConnection();

        try {
            User user = this.getUserByUsername(savedUser.getUsername());

            if (user == null) {
                PreparedStatement saveUser = connection.prepareStatement(
                        "insert into users(username, password, birthDate, email, phoneNumber, gender, role)" +
                                "VALUES('" + savedUser.getUsername() + "','" +
                                savedUser.getPassword() + "','" +
                                savedUser.getBirthDate().toString() + "','" +
                                savedUser.getEmail() + "','" +
                                savedUser.getPhoneNumber() + "','" +
                                savedUser.getGender() + "'," +
                                savedUser.getRoleCode() + ")");
                saveUser.executeUpdate();
                savedUser = this.getUserByUsername(savedUser.getUsername());
                this.savePatient(savedUser.getId());
                return savedUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Patient savePatient(int userId) {
        Connection connection = this.getConnection();
        Patient savedPatient = new Patient();

        try {
            PreparedStatement savePatient = connection.prepareStatement(
                    "insert into patients(idUser, height, weight, bloodType, remark)" +
                            "VALUES(" + userId + "," +
                            savedPatient.getHeight() + "," +
                            savedPatient.getWeight() + ",'" +
                            savedPatient.getBloodType() + "','" +
                            savedPatient.getRemark() + "')");
            savePatient.executeUpdate();
            return savedPatient;
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Patient deletePatient(Patient deletedPatient) {
        Connection connection = this.getConnection();

        try {
            PreparedStatement deletePatient = connection.prepareStatement(
                    "DELETE FROM patients" +
                            " WHERE patients.idPatient = " + deletedPatient.getIdPatient());
            deletePatient.executeUpdate();
            return deletedPatient;
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Patient getPatientByUserId(int userId) {
        Connection connection = this.getConnection();

        try {
            Patient patient = new Patient();
            PreparedStatement saveUser = connection.prepareStatement(
                    "SELECT * FROM patients" +
                            " where patients.idUser = " + userId);
            ResultSet rs = saveUser.executeQuery();

            if (rs.next()) {
                patient.setIdPatient(rs.getInt(1));
                patient.setIdUser(rs.getInt(2));
                patient.setHeight(rs.getFloat(3));
                patient.setWeight(rs.getFloat(4));
                patient.setBloodType(rs.getString(5));
                patient.setRemark(rs.getString(6));
            }
            return patient;
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public User deleteUser(User deletedUser) {
        Connection connection = this.getConnection();

        try {
            User user = this.getUserByUsername(deletedUser.getUsername());

            if (user != null) {
                PreparedStatement saveUser = connection.prepareStatement(
                        "DELETE FROM users" +
                                " WHERE users.username = '" + deletedUser.getUsername() + "'");
                saveUser.executeUpdate();
                return deletedUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public List<PatientData> getPatientDataList() {
        List<PatientData> patientDataList = new ArrayList<>();
        PatientData patientData;
        Connection connection = this.getConnection();

        try {
            PreparedStatement getPatientDataList = connection.prepareStatement(
                    "SELECT p.idPatient, u.username, u.birthDate, u.gender, p.height," +
                            "p.weight, p.bloodType, p.remark " +
                            "FROM users u, patients p " +
                            "WHERE u.id = p.idPatient"
            );
            ResultSet rs = getPatientDataList.executeQuery();

            while (rs.next()) {
                patientData = new PatientData();
                patientData.setIdPatient(rs.getInt(1));
                patientData.setName(rs.getString(2));
                patientData.setAge(this.calculateAge(rs.getDate(3)));
                patientData.setGender(rs.getString(4));
                patientData.setHeight(rs.getFloat(5));
                patientData.setWeight(rs.getFloat(6));
                patientData.setBloodType(rs.getString(7));
                patientData.setRemark(rs.getString(8));
                patientDataList.add(patientData);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return patientDataList;
    }

    public Patient getPatientById(int patientId) {
        Patient patient;
        Connection connection = this.getConnection();

        try {
            PreparedStatement getPatientDataList = connection.prepareStatement(
                    "SELECT * FROM patients " +
                            "WHERE patients.idPatient = " + patientId
            );
            ResultSet rs = getPatientDataList.executeQuery();

            if (rs.next()) {
                patient = new Patient();
                patient.setIdPatient(rs.getInt(1));
                patient.setIdUser(rs.getInt(2));
                patient.setHeight(rs.getFloat(3));
                patient.setWeight(rs.getFloat(4));
                patient.setBloodType(rs.getString(5));
                patient.setRemark(rs.getString(6));
                return patient;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public List<PatientComboBoxData> getPatientComboBoxList() {
        List<PatientComboBoxData> patientList = new ArrayList<>();
        PatientComboBoxData patient;
        Connection connection = this.getConnection();

        try {
            PreparedStatement getPatientList = connection.prepareStatement(
                    "SELECT p.idPatient, u.username FROM patients p, users u WHERE p.idUser = u.id"
            );
            ResultSet rs = getPatientList.executeQuery();

            while (rs.next()) {
                patient = new PatientComboBoxData();
                patient.setId(rs.getInt(1));
                patient.setName(rs.getString(2));
                patientList.add(patient);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return patientList;
    }

    public List<Consultancy> getConsultancyList() {
        List<Consultancy> consultancyList = new ArrayList<>();
        Consultancy consultancy;
        Connection connection = this.getConnection();

        try {
            PreparedStatement getConsultancyList = connection.prepareStatement(
                    "SELECT * FROM consultancies"
            );
            ResultSet rs = getConsultancyList.executeQuery();

            while (rs.next()) {
               consultancy = new Consultancy();
               consultancy.setId(rs.getInt(1));
               consultancy.setName(rs.getString(2));
               consultancy.setPrice(rs.getFloat(3));
               consultancy.setDomain(rs.getString(4));
               consultancyList.add(consultancy);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return consultancyList;
    }

    public List<String> getConsultancyDomainList() {
        List<String> consultancyDomainList = new ArrayList<>();
        Connection connection = this.getConnection();

        try {
            PreparedStatement getConsultancyList = connection.prepareStatement(
                    "SELECT DISTINCT consultancies.domain FROM consultancies"
            );
            ResultSet rs = getConsultancyList.executeQuery();

            while (rs.next()) {
                consultancyDomainList.add(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return consultancyDomainList;
    }

    public Doctor getDoctorByUserId(int userId) {
        Doctor doctor = new Doctor();
        Connection connection = this.getConnection();

        try {
            PreparedStatement getDoctorByUserId = connection.prepareStatement(
                    "SELECT * FROM doctors where doctors.idUser = '" +  userId + "'"
            );
            ResultSet rs = getDoctorByUserId.executeQuery();
            if (rs.next()) {
                doctor.setId(rs.getInt(1));
                doctor.setIdUser(rs.getInt(2));
                doctor.setDomain(rs.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return doctor;
    }

    public List<Analysis> getAnalysisList(int idDoctor) {
        List<Analysis> analysisList = new ArrayList<>();
        Analysis analysis;
        Connection connection = this.getConnection();

        try {
            PreparedStatement getAnalysisList = connection.prepareStatement(
                    "SELECT a.idAnalysis, a.idDoctor, a.idPatient, u.username, a.idConsultancy, " +
                            " c.name, a.analysisDate, a.domain, a.remark" +
                            " FROM analyses a, patients p, consultancies c, users u " +
                            " where a.idDoctor='" + idDoctor + "' and " +
                            " a.idPatient = p.idPatient and a.idConsultancy = c.idConsultancy and" +
                            " u.id = p.idUser"
            );
            ResultSet rs = getAnalysisList.executeQuery();

            while (rs.next()) {
                analysis = new Analysis();
                analysis.setId(rs.getInt(1));
                analysis.setIdDoctor(rs.getInt(2));
                analysis.setIdPatient(rs.getInt(3));
                analysis.setPatientName(rs.getString(4));
                analysis.setIdConsultancy(rs.getInt(5));
                analysis.setConsultancyName(rs.getString(6));
                analysis.setAnalysisDate(rs.getDate(7));
                analysis.setDomain(rs.getString(8));
                analysis.setRemark(rs.getString(9));
                analysisList.add(analysis);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return analysisList;
    }

    public Analysis saveAnalysis(Analysis savedAnalysis) {
        Connection connection = this.getConnection();

        try {
            if (savedAnalysis != null) {
                PreparedStatement saveAnalysis = connection.prepareStatement(
                        "insert into analyses(idDoctor, idPatient, idConsultancy, analysisDate, domain, remark)" +
                                "VALUES(" + savedAnalysis.getIdDoctor() + "," + savedAnalysis.getIdPatient() +
                                "," + savedAnalysis.getIdConsultancy() + ",'" +
                                savedAnalysis.getAnalysisDate().toString() + "','" + savedAnalysis.getDomain() +
                                "','" + savedAnalysis.getRemark() + "')");
                saveAnalysis.executeUpdate();
                return savedAnalysis;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Analysis deleteAnalysis(Analysis deletedAnalysis) {
        Connection connection = this.getConnection();

        try {
            if (deletedAnalysis != null) {
                PreparedStatement deleteAnalysis = connection.prepareStatement(
                        "DELETE FROM analyses" +
                                " WHERE analyses.idAnalysis = " + deletedAnalysis.getId());
                deleteAnalysis.executeUpdate();
                return deletedAnalysis;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Consultancy saveConsultancy(Consultancy savedConsultancy) {
        Connection connection = this.getConnection();

        try {
            if (savedConsultancy != null) {
                PreparedStatement saveConsultancy = connection.prepareStatement(
                        "insert into consultancies(name, price, domain)" +
                                "VALUES('" + savedConsultancy.getName() + "'," + savedConsultancy.getPrice() +
                                ",'" + savedConsultancy.getDomain() + "')");
                saveConsultancy.executeUpdate();
                return savedConsultancy;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public Consultancy deleteConsultancy(Consultancy deletedConsultancy) {
        Connection connection = this.getConnection();

        try {
            if (deletedConsultancy != null) {
                PreparedStatement deleteConsultancy = connection.prepareStatement(
                        "DELETE FROM consultancies" +
                                " WHERE consultancies.idConsultancy = " + deletedConsultancy.getId());
                deleteConsultancy.executeUpdate();
                return deletedConsultancy;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return null;
    }

    public int getSavedAnalysisId() {
        Connection connection = this.getConnection();

        try {
            PreparedStatement getSavedAnalysisId = connection.prepareStatement(
                    "SELECT Count(analyses.idAnalysis) FROM analyses"
            );
            ResultSet rs = getSavedAnalysisId.executeQuery();

            if(rs.isBeforeFirst()) {
                rs.next();
            }
            
            return rs.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return 0;
    }

    public int getSavedConsultancyId() {
        Connection connection = this.getConnection();

        try {
            PreparedStatement getSavedConsultancyId = connection.prepareStatement(
                    "SELECT Count(consultancies.idConsultancy) FROM consultancies"
            );
            ResultSet rs = getSavedConsultancyId.executeQuery();

            if(rs.isBeforeFirst()) {
                rs.next();
            }

            return rs.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        } finally {
            this.close(connection);
        }

        return 0;
    }

    private User decomposeUserAttributes(ResultSet rs) {
        User user = new User();

        try {
            user.setId(rs.getInt(1));
            user.setUsername(rs.getString(2));
            user.setPassword(rs.getString(3));
            user.setBirthDate(rs.getDate(4));
            user.setEmail(rs.getString(5));
            user.setPhoneNumber(rs.getString(6));
            user.setGender(rs.getString(7));
            user.setRoleCode(rs.getInt(8));
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        }

        return user;
    }

    private int calculateAge(Date birthDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(calendar.getTime());
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.setTime(birthDate);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);
        int age = currentYear - year;

        if (currentMonth < month) {
            age--;
        }

        if (currentMonth == month && currentDay < day) {
            age--;
        }

        return age;
    }

    protected Connection getConnection() {
        try {
            Class.forName(databaseConfig.getProperty("datasource.driver"));
            return DriverManager.getConnection(databaseConfig.getProperty("datasource.url"),
                                                     databaseConfig.getProperty("datasource.username"),
                                                     databaseConfig.getProperty("datasource.password"));
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        }

        return null;
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
        }
    }
}
