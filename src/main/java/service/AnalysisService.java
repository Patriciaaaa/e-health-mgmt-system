package service;

import javafx.scene.control.Alert;
import model.Analysis;
import util.AlertProvider;
import util.ResourceProvider;
import util.StringProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class AnalysisService {
    private SqlConnectorService sqlConnectorService;

    public AnalysisService() {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
            return;
        }

        this.sqlConnectorService = new SqlConnectorService(properties);
    }

    public List<Analysis> getAnalysisList(int idDoctor) {
        return this.sqlConnectorService.getAnalysisList(idDoctor);
    }

    public Analysis saveAnalysis(Analysis analysis) {
        Analysis savedAnalysis = this.sqlConnectorService.saveAnalysis(analysis);

        if (savedAnalysis != null) {
            savedAnalysis.setId(this.sqlConnectorService.getSavedAnalysisId());
        }

        return savedAnalysis;
    }

    public SqlConnectorService getSqlConnectorService() {
        return sqlConnectorService;
    }

    public void setSqlConnectorService(SqlConnectorService sqlConnectorService) {
        this.sqlConnectorService = sqlConnectorService;
    }
}
