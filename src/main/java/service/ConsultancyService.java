package service;

import javafx.scene.control.Alert;
import model.Consultancy;
import util.AlertProvider;
import util.ResourceProvider;
import util.StringProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ConsultancyService {
    private SqlConnectorService sqlConnectorService;

    public ConsultancyService() {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
            AlertProvider.displayAlert(
                    "Error",
                    StringProvider.contactAdministrator,
                    Arrays.toString(e.getStackTrace()),
                    Alert.AlertType.ERROR
            );
            return;
        }

        this.sqlConnectorService = new SqlConnectorService(properties);
    }

    public List<Consultancy> getConsultancyList() {
        return this.sqlConnectorService.getConsultancyList();
    }

    public List<String> getConsultancyDomainList() {
        return this.sqlConnectorService.getConsultancyDomainList();
    }

    public Consultancy saveConsultancy(Consultancy consultancy) {
        Consultancy savedConsultancy = this.sqlConnectorService.saveConsultancy(consultancy);

        if (savedConsultancy != null) {
            savedConsultancy.setId(this.sqlConnectorService.getSavedConsultancyId());
        }

        return savedConsultancy;
    }

    public SqlConnectorService getSqlConnectorService() {
        return sqlConnectorService;
    }

    public void setSqlConnectorService(SqlConnectorService sqlConnectorService) {
        this.sqlConnectorService = sqlConnectorService;
    }
}
