import model.User;
import service.SqlConnectorService;
import util.ResourceProvider;

import java.util.List;
import java.util.Properties;

//class used for testing the database connection, to start the application use the ApplicationStarter class.
public class ServiceStarter {
    public static void main(String[] args) {
        Properties properties = new Properties();

        try {
            properties.load(ResourceProvider.loadConfiguration());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        SqlConnectorService sqlConnector = new SqlConnectorService(properties);
        List<User> userList = sqlConnector.getUsers();
        userList.forEach(user -> System.out.println(user.toString()));
    }
}
