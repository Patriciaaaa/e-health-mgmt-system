package model;

import util.StringProvider;

import java.sql.Date;

public class User {
    private int id;
    private String username;
    private String password;
    private Date birthDate;
    private String email;
    private String phoneNumber;
    private String gender;
    private int roleCode;

    public User() {
        username = "";
        password = "";
        birthDate = new Date(0);
        email = "";
        phoneNumber = "";
        gender = "";
    }

    public User(int id, String username, String password, int roleCode) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roleCode = roleCode;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRoleString() {
        if (this.roleCode == 2) {
            return StringProvider.ROLE_PATIENT;
        }

        return StringProvider.ROLE_DOCTOR;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", birthDate=" + birthDate +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", roleCode=" + roleCode +
                '}';
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode();
    }
}
