package model;

import java.sql.Date;

public class Analysis {
    private int id;
    private int idDoctor;
    private int idPatient;
    private String patientName;
    private int idConsultancy;
    private String consultancyName;
    private Date analysisDate;
    private String domain;
    private String remark;

    public Analysis() {
        this.patientName = "";
        this.consultancyName = "";
        this.analysisDate = new Date(0);
        this.domain = "";
        this.remark = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getIdConsultancy() {
        return idConsultancy;
    }

    public void setIdConsultancy(int idConsultancy) {
        this.idConsultancy = idConsultancy;
    }

    public String getConsultancyName() {
        return consultancyName;
    }

    public void setConsultancyName(String consultancyName) {
        this.consultancyName = consultancyName;
    }

    public Date getAnalysisDate() {
        return analysisDate;
    }

    public void setAnalysisDate(Date analysisDate) {
        this.analysisDate = analysisDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Analysis{" +
                "id=" + id +
                ", idDoctor=" + idDoctor +
                ", idPatient=" + idPatient +
                ", patientName=" + patientName +
                ", idConsultancy=" + idConsultancy +
                ", consultancyName=" + consultancyName +
                ", analysisDate=" + analysisDate +
                ", domain='" + domain + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode();
    }
}
