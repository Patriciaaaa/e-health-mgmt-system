package model;

public class Doctor {
    private int id;
    private int idUser;
    private String domain;

    public Doctor() {
        this.domain = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", domain='" + domain + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return 17 * super.hashCode();
    }
}
