package model;

public class Consultancy {
    private int id;
    private String name;
    private float price;
    private String domain;

    public Consultancy() {
        this.name = "";
        this.domain = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "Consultancy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", domain='" + domain + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return 13 * super.hashCode();
    }
}
